# Games VPN
Small docker-compose project to play to old games through a VPN. This project include Teamspeak server for communication and
Samba server for share files.

[[_TOC_]]

## Requirements
| Requirements   | Versions  |
| -------------: | :-------- |
| Linux/Ubuntu   | 20.04 LTS |
| Docker         | 20.10     |
| Docker-compose | 1.28      |
| GNU/Make       | 4.3       |

## Setup remote machine
- rent a Digital Ocean's Droplet 

- generate a new SSH Key
  ```bash
  ssh-keygen -t ed25519 -C "<your_email>"
  ```

- activate ssh connection with your SSH key

- on Digital Ocean dash activate firewall:
  - allow outbound traffic
  - filter outbound 
  - add rule to open ssh port (22) limited to your ip
  - add rule to open VPN port (51820 by default for wireguard)

- open SSH connection with your droplet with 
  ```bash
  ssh root@<droplet_ip>
  ```

- create remote user
  ```bash
  adduser -m -G sudo <user_name>
  passwd <user_name>
  ```

- switch to remote user (more secure)
  ```bash
  su <user_name>
  ```
 
- add your public SSH key to use your remote user
  ```bash
  # On your local machine
  ssh-copy-id -i ~/.ssh/<your_public_key> <user_name>@<droplet_ip>
  ```

- update and upgrade OS packages
  ```bash
  sudo apt update
  sudo apt upgrade
  ```

- edit `/etc/ssh/sshd_config` with `vim` or `nano`
  ```bash
  vim /etc/ssh/sshd_config
  ```

  ```config
  # disable connection with root
  PermitRootLogin no

  # allow only your remote user to open connection
  AllowUsers <user_name>

  # Disable authentification with password
  PasswordAuthentication no
  ```

- reload SSH service
  ```bash
  sudo systemctl restart sshd.service
  ```

- install `fail2ban`
  ```bash
  sudo apt install fail2ban
  ```

- copy `/etc/fail2ban.jail.conf` to `/etc/fail2ban.jail.local`
  ```bash
  sudo cp /etc/fail2ban/jail.{conf,local}
  ```

- edit `/etc/fail2ban/jail.local`
  ```
  ignoreip = <your_ip> <second_ip> # ip or network to ignore

  # Ban settings
  bantime = <time_to_ban | example = 10m> # time in second to ban ip
  findtime = <time | example = 10m> # time between to failure 
  maxretry = <retries | example = 3 | default=5> # number of failure allow before ban

  # Jails
  [<jail_name | example=sshd>]
  enable = true
  port = 22

  # email notifications (required smtp server)
  action = %(action_mw)s # ban ip and send email (action_mwl allows to include log in email)
  destemail = # set destination of the email
  sender = # set sender email
  ```

- start service
  ```bash
  sudo systemctl start fail2ban
  ```

- install `git`
  ```bash
  sudo apt install git
  ```

- install [Docker](https://docs.docker.com/engine/install/ubuntu/) and
[Docker-compose](https://docs.docker.com/compose/install/)

## Install
- download the project with `git`
  ```bash
  cd <project_folder>
  git clone https://gitlab.com/LouisBruge/games-vpn.git .
  ```

- generate `.env` file that will contains private env variables
  ```bash
  cd <project_folder>
  echo "HOST_IP=<droplet ip>\nPEERS=<num_vpn_clients>" > .env
  ```

## Start 
- go to project folder
  ```bash
  cd <project_folder>
  ```

- start docker stack
  ```bash
  sudo make start
  ```

- retrieve peers keys and config on folder `/etc/wireguard/config` and download them on your local desktop
  ```bash
  scp <user_name>@<droplet_ip>:/etc/wireguard/config/peer_<number> ~/.
  ```

## Usage
### VPN Connection
#### Linux
- install wireguard
  ```bash
  sudo apt install wireguard
  ```

- retrieve public and private key and config
  ```bash
  mkdir /tmp/wireguard/
  cd ~/wireguard/
  scp -r <user_name>@<droplet_ip>:/etc/wireguard/config/peer_<number> .
  sudo cp privatekey /etc/wireguard/
  sudo cp publickey /etc/wireguard/
  sudo cp wg0.conf /etc/wireguard/
  sudo chown -Rv /etc/wireguard/
  sudo chmod -Rv 700 /etc/wireguard/
  sudo rm -rv ~/wireguard
  ```

- connect the server
  ```bash
  sudo wg-quick wg0
  ```

- check the connection
  ```bash
  sudo wg wg0
  ```
##### Windows
- download windows client [here](https://www.wireguard.com/install/)

- launch the client

- click on `Add Tunnel`

- click on `Import tunnel(s) from file...`

- drop files

- click on connect

#### Available services on VPN
- The teamspeak server will be accessible at `teamspeak.vpn` (see
      https://support.teamspeak.com/hc/en-us/articles/360002712237-What-is-the-default-permission-set-in-a-new-TeamSpeak-3-server-installation-)

- The samba server will be accessible at `samba.vpn`

- The grafana server will be accessible at `http://grafana.vpn:3000` url (default user: admin, default password:
  admin)

## Remove
- go to project foler
  ```bash
  cd <project_folder>
  ```

- stop docker stack
  ```bash
  sudo make remove
  ```

## Internal network architecture
```mermaid
flowchart TB
    A(peer A)
    B(peer B)
    C(peer C)
    DNS(DNS Server)
    W(Wireguard Server)
    S(Samba Server)
    T(Teamspeak Server)
    G(Grafana)
    P(Prometheus)
    CA(Cadvisor)
    N(Node Exporter)



    subgraph VPN [192.168.3.0/24]
      A
      B
      C
      W
    end

    subgraph Service [172.28.0.0/16]
    W
    S
    T
    G
    DNS
    end

    subgraph Monitoring [172.x.x.x/16]
        G
        P
        CA
        N
    end


    A <-.-> B
    A <-.-> C
    B <-.-> C

    A <---> W
    B <---> W
    C <---> W

    W ---> DNS
    W ---> S
    W ---> T
    W ---> G
    G --> P
    P --> CA
    P --> N
```
