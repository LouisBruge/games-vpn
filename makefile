PROJECT ?= vpn

LOGS_TAIL ?= 100

DOCKER_COMPOSE = docker-compose -f docker-compose.yml -p $(PROJECT)

.DEFAULT_GOAL := help

SUPPORTED_COMMANDS := wg logs start stop remove pull update restart events
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))

ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  # use the rest as arguments for the command
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(COMMAND_ARGS):;@:)
endif

define echo
	@tput setaf 11
	@echo "[$(PROJECT)] - $(1)"
	@tput sgr0
endef

define echosuccess
	@tput setaf 40
	@echo "[$(PROJECT)] - $(1)"
	@tput sgr0
endef

.PHONY: help
help: ## print help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: ## start all containers or containers send as arg
	@$(call echo,starting containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) up -d --remove-orphans $(COMMAND_ARGS)
	@$(call echosuccess,starting containers $(COMMAND_ARGS) done !)

.PHONY: stop
stop: ## stop all containers or containers send as arg
	@$(call echo,stoping containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) stop $(COMMAND_ARGS)
	@$(call echosuccess,stoping containers $(COMMAND_ARGS) done !)

.PHONY: restart
restart: ## stop and restarts containers or containers send as arg
	@$(call echo,restart containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) restart $(COMMAND_ARGS)
	@$(call echosuccess,restart containers $(COMMAND_ARGS) done !)

.PHONY: events
events: ## list events related to containers or containers send as arg
	@$(call echo,listing containers $(COMMAND_ARGS) events please wait...)
	@$(DOCKER_COMPOSE) events $(COMMAND_ARGS)
	@$(call echosuccess,listing containers $(COMMAND_ARGS) events done !)

.PHONY: update
update: ## update services images

.PHONY: pull
pull: ## update services images
	@$(call echo,pulling containers $(COMMAND_ARGS) images please wait...)
	@$(DOCKER_COMPOSE) pull --include-deps $(COMMAND_ARGS)
	@$(call echosuccess,pulling containers $(COMMAND_ARGS) images done !)

.PHONY: upgrade
upgrade: pull restart ## update services images and restart containers


.PHONY: remove
remove: ## stop and remove all containers or containers send as arg
	@$(call echo,removing containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) down --remove-orphans $(COMMAND_ARGS)
	@$(call echosuccess,removing containers $(COMMAND_ARGS) done !)

.PHONY: top
top: ## List running processes for all containers or containers send as arg
	@$(call echo,listing running processes for containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) top $(COMMAND_ARGS)
	@$(call echosuccess,listing running processes for containers $(COMMAND_ARGS) done !)


.PHONY: status
status: ## List containers
	@$(call echo,starting display containers $(COMMAND_ARGS) status please wait...)
	@$(DOCKER_COMPOSE) ps

.PHONY: logs
logs: ## display logs for all containers or containers send as arg
	@$(call echo,starting display logs for containers $(COMMAND_ARGS) please wait...)
	@$(DOCKER_COMPOSE) logs --tail=$(LOGS_TAIL) -f $(COMMAND_ARGS)

.PHONY: wg
wg: ## interact with wireguard
	@$(DOCKER_COMPOSE) exec wireguard wg $(COMMAND_ARGS)
